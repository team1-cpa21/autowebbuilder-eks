# add the cluster to the 2 subnets
resource "aws_db_subnet_group" "db" {
  subnet_ids = aws_subnet.db[*].id
}

resource "aws_rds_cluster" "cluster" {
  engine                 = "aurora-mysql"
  engine_version         = "5.7.mysql_aurora.3.02.1"
  engine_mode            = "serverless"
  database_name          = "mydb"
  master_username    = var.db_username
  master_password    = var.db_password
  enable_http_endpoint   = true
  skip_final_snapshot    = true
	# attach the security group
  vpc_security_group_ids = [aws_security_group.db.id]
	# deploy to the subnets
  db_subnet_group_name   = aws_db_subnet_group.db.name
}

