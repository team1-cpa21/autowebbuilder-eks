variable "cluster_name" {
  default = "kubeautoweb"
}

variable "cluster_version" {
  default = "1.22"
}
variable "db_username" {
  description = "Database administrator username"
  type        = string
  sensitive   = true
}

variable "db_password" {
  description = "Database administrator password"
  type        = string
  sensitive   = true
}